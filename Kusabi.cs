﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using YamlDotNet.RepresentationModel;
using SimpleLog;

namespace Kusabi {
    class Kusabi {
        readonly Log log;

        // YAML
        protected StreamReader input;                       // file input
        protected YamlStream yaml;                          // YAML
        protected YamlSequenceNode yamlSequenceNode;        // sequence node
        IList<YamlNode> spiders;                            // spider list

        // maximum number of crawl retries
        private readonly int maxCrawlRetries = 3;

        // task
        Task<(bool crawlResult, int crawlTotal, int crawlError)> yahooTask;
        Task<(bool crawlResult, int crawlTotal, int crawlError)> itmediaTask;

        // task result
        bool yahooResult;
        bool itmediaResult;

        // number of downloaded html files for each task
        int yahooTotal;
        int itmediaTotal;

        // number of errors for each task
        int yahooError;
        int itmediaError;

        // constructor
        public Kusabi() {
            log = new Log("kusabi");
        }

        // Read spider list.
        public void GetSpiderList(string yamlFileName, string yamlFileDirectory = "./") {
            input = new StreamReader($"{yamlFileDirectory}{yamlFileName}", Encoding.UTF8);
            yaml = new YamlStream();
            yaml.Load(input);
            yamlSequenceNode = (YamlSequenceNode)yaml.Documents[0].RootNode;
            spiders = yamlSequenceNode.Children;

            foreach (YamlNode spider in spiders) {
                string prefecture = spider.ToString();
                log.Write($"Set {prefecture} Spider.");
            }
        }

        public void ExecTasks() {
            log.Write("Kusabi Start!", "w");

            // Get spider list.
            GetSpiderList("SpiderList.yml");

            // Launch tasks.
            if (spiders.Contains("yahoo")) {
                System.Threading.Thread.Sleep(2000);
                yahooTask = Task.Run(Yahoo);
            }

            if (spiders.Contains("itmedia")) {
                System.Threading.Thread.Sleep(2000);
                itmediaTask = Task.Run(Itmedia);
            }

            // Wait for the task to finish.
            if (spiders.Contains("yahoo")) {
                yahooTask.Wait();
                (yahooResult, yahooTotal, yahooError) = yahooTask.Result; 
                log.Write($"yahoo: {yahooResult}, {yahooTotal}, {yahooError}");
            }
            
            if (spiders.Contains("itmedia")) {
                itmediaTask.Wait();
                (itmediaResult, itmediaTotal, itmediaError) = itmediaTask.Result; 
                log.Write($"itmedia: {itmediaResult}, {itmediaTotal}, {itmediaError}");
            }

            log.Write("Kusabi End!");
        }

        public (bool crawlResult, int crawlTotal, int crawlError) Yahoo() {
            Yahoo yahoo;
            bool crawlResult = false;
            int crawlTotal = 0;
            int crawlError = 0;

            for (int i = 0; i < maxCrawlRetries; i++) { 
                yahoo = new Yahoo("yahoo");
                (crawlResult, crawlTotal, crawlError) = yahoo.Crawl();
                if (crawlResult) { 
                    break;
                } else {
                    System.Threading.Thread.Sleep(600000);
                    log.Write($"yahoo retry count: {i}");                
                }
            }
            return (crawlResult, crawlTotal, crawlError);
        }

        public (bool crawlResult, int crawlTotal, int crawlError) Itmedia() {
            Itmedia itmedia;
            bool crawlResult = false;
            int crawlTotal = 0;
            int crawlError = 0;

            for (int i = 0; i < maxCrawlRetries; i++) { 
                itmedia = new Itmedia("itmedia");
                (crawlResult, crawlTotal, crawlError) = itmedia.Crawl();
                if (crawlResult) { 
                    break;
                } else {
                    System.Threading.Thread.Sleep(600000);
                    log.Write($"itmedia retry count: {i}");                
                }
            }
            return (crawlResult, crawlTotal, crawlError);
        }

    }
}
