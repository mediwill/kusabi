﻿using Nona;
using System;
using System.IO;
using OpenQA.Selenium.Chrome;
using System.Reflection;

namespace Kusabi
{
    class Yahoo : Nona.BasicSpider, ISpider {
        
        public Yahoo(string crawlerName) : base(crawlerName, false) {
            ReadYaml($"{name}.yml", $"{homeDirectory}spiders/conf/");
            GetSpiderConf();
            driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), options);
        }

        public (bool, int, int) Crawl() {
            log.Write($"{name} Start!");

            try { 
                CheckSwap();
                GetStartPage(startUrl);
                GetPages();
            } catch (Exception e) {
                MakeSwap();
                log.Write($"{name} Exception Error!!");
                log.Write(e.Message);
                log.Write(e.StackTrace);
                crawlResult = false;
            }
         
            log.Write($"{name} End!");
            return (crawlResult, fileNumber - 1, crawlError);
        }
        
        public void GetSpiderConf() {
            string intervalTimeString = GetYamlMappingValue("INTERVAL_TIME", yamlMappingNode);
            IntervalTime = int.Parse(intervalTimeString);
            startUrl = GetYamlMappingValue("START_URL", yamlMappingNode);
        }
        
        private void GetPages() {

            fileNumber = 1;
            for (int i = 1; i <= 8; i++) {                                             
                ClickElementXPath($"//*[@id=\"tabpanelTopics1\"]/div/div[1]/ul/li[{i}]/article/a/div/div/h1/span");

                Sleep(IntervalTime);
                html = driver.PageSource;
                WriteHtml($"{name}_{fileNumber:000000}.html");
                WriteScreenshotDebug($"{name}_{fileNumber:000000}.png");

                // back to start page
                HistoryBackWindow();
                fileNumber++;
            }
        }
    }
}
