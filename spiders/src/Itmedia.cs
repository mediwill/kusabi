﻿using Nona;
using System;
using System.IO;
using OpenQA.Selenium.Chrome;
using System.Reflection;

namespace Kusabi
{
    class Itmedia : Nona.BasicSpider, ISpider {
        
        public Itmedia(string crawlerName) : base(crawlerName) {
            ReadYaml($"{name}.yml", $"{homeDirectory}spiders/conf/");
            GetSpiderConf();
            driver = new ChromeDriver(Path.GetDirectoryName(Assembly.GetEntryAssembly().Location), options);
        }

        public (bool, int, int) Crawl() {
            log.Write($"{name} Start!");

            try { 
                CheckSwap();
                GetStartPage(startUrl);

                WriteScreenshotDebug("start_page.png");

                GetPages();
            } catch (Exception e) {
                MakeSwap();
                log.Write($"{name} Exception Error!!");
                log.Write(e.Message);
                log.Write(e.StackTrace);
                crawlResult = false;
            }
         
            log.Write($"{name} End!");
            return (crawlResult, fileNumber - 1, crawlError);
        }
        
        public void GetSpiderConf() {
            string intervalTimeString = GetYamlMappingValue("INTERVAL_TIME", yamlMappingNode);
            IntervalTime = int.Parse(intervalTimeString);
            startUrl = GetYamlMappingValue("START_URL", yamlMappingNode);
        }
        
        private void GetPages() {

            fileNumber = 1;
            for (int ranking = 1; ranking <= 10; ranking++) {                             
                ClickElement($"#colBoxRanking > div.colBoxTab1 > div > div.colBoxUlist > ul > li.rank{ranking} > a");

                Sleep(IntervalTime);
                html = driver.PageSource;
                WriteHtml($"{name}_{fileNumber:000000}.html");             
                WriteScreenshotDebug($"{name}_{fileNumber:000000}.png");

                // back to start page
                HistoryBackWindow();
        
                Sleep(IntervalTime);
                fileNumber++;
            }
        }
    }
}
